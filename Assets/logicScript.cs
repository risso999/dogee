using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class logicScript : MonoBehaviour
{
    public Text scoreText;
    public int initialScore = 0;
    public GameObject gameOverScreen;

    public void Start()
    {
        scoreText.text = initialScore.ToString();
    }

    // [ContextMenu("Increase Score")]
    public void addScore(int increment)
    {
        int score = int.Parse(scoreText.text);
        score += increment;
        scoreText.text = score.ToString();
    }

    public void restartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void gameOver()
    {
        gameOverScreen.SetActive(true);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pipeSpawnScript : MonoBehaviour
{
    public GameObject[] pipes;
    public float spawnRate = 2;
    public float timer = 0;
    public float offset = 6;
    // Start is called before the first frame update
    void Start()
    {
        spawn();
    }

    // Update is called once per frame
    void Update()
    {
        if (timer < spawnRate)
        {
            timer += Time.deltaTime;
        }
        else 
        {
            spawn();
            timer = 0;
        }
        
    }

    void spawn()
    {
        int pipe = Random.Range(0,3);
        Instantiate(pipes[pipe], transform.position + Vector3.up * Random.Range(0, offset), transform.rotation);
    }

}

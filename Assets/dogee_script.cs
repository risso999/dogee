using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dogee_script : MonoBehaviour
{
    public Rigidbody2D body;
    public float jump;
    public logicScript logic;
    public bool dogeeAlive = true;
    public float deadZone = 6;
    public AudioSource dogSound;
    public AudioSource dogWalkSound;
    public AudioSource dogOverSound;

    // Start is called before the first frame update
    void Start()
    {
        logic = GameObject.FindGameObjectWithTag("Logic").GetComponent<logicScript>();
        dogWalkSound.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && dogeeAlive)
        {
            dogSound.Play();
            body.velocity = Vector2.up * jump;
        }
        if (transform.position.y < -deadZone || transform.position.y > deadZone)
        {
            dogeeOver();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        dogeeOver();
    }

    private void dogeeOver()
    {
        if (dogeeAlive)
        {
            logic.gameOver();
            dogeeAlive = false;
            dogWalkSound.Stop();
            dogOverSound.Play();
        }
    }
}
